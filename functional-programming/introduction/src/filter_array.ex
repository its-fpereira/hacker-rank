defmodule FilterArray do
  [s | tail] = String.split(IO.read(:stdio, :all), [" ", "\n"])
  {k, _} = Integer.parse(s)
  Enum.map(
    tail,
    fn x ->
      {i, _} = Integer.parse(x)
      if i < k, do: IO.puts(x)
    end
  )
end
