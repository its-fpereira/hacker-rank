import java.util.*
import kotlin.collections.ArrayList

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val arr = ArrayList<String>()

    (0..(n - 1)).map { arr.add(scanner.next()) }

    val q = scanner.nextInt()

    (0..(q - 1)).map {
        val str = scanner.next()
        println(arr.filter { s -> s == str }.size)
    }
}