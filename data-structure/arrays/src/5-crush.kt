import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val m = scanner.nextInt()
    val arr = LongArray(n)

    (0 until m).map {
        val a = scanner.nextInt() - 1
        val b = scanner.nextInt() - 1
        val k = scanner.nextLong()

        arr[a] += k
        if (b + 1 < n) arr[b + 1] -= k
    }

    var max = 0L
    var temp = 0L

    for (i in 0 until n) {
        temp += arr[i]
        if (temp > max) max = temp
    }

    print(max)
}