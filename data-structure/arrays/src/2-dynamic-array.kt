import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val sequences = HashMap<Int, ArrayList<Int>>()
    val q = scanner.nextInt()

    for (i in 0..(n - 1)) sequences.put(i, ArrayList())

    var lastAnswer = 0
    for (arr_i in 0..(q - 1)) {
        val typeQuery = scanner.nextInt()
        val x = scanner.nextInt()
        val y = scanner.nextInt()
        val seq = sequences.get(x.xor(lastAnswer) % n)!!

        when (typeQuery) {
            1 -> seq.add(y)
            2 -> {
                lastAnswer = seq.get(y % seq.size)
                println(lastAnswer)
            }
        }
    }
}