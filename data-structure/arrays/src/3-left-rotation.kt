import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val d = scanner.nextInt()
    val arr = IntArray(n)

    (0..(n - 1)).map { i ->
        var leftRotationNewIndex = i - d
        if (leftRotationNewIndex < 0) leftRotationNewIndex += n
        arr[leftRotationNewIndex] = scanner.nextInt()
    }

    arr.map { i -> print("$i ") }
}