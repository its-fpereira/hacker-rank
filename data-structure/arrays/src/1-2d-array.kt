import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val arr = Array(6) { IntArray(6) }
    for (arr_i in 0..5) {
        for (arr_j in 0..5) {
            arr[arr_i][arr_j] = scanner.nextInt()
        }
    }

    var maxSum = Integer.MAX_VALUE
    for (arr_i in 0..5) {
        for (arr_j in 0..5) {
            if (arr_i + 2 <= 5 && arr_j + 2 <= 5) {
                val sum = arr[arr_i][arr_j] + arr[arr_i][arr_j + 1] + arr[arr_i][arr_j + 2] + arr[arr_i + 1][arr_j + 1] + arr[arr_i + 2][arr_j] + arr[arr_i + 2][arr_j + 1] + arr[arr_i + 2][arr_j + 2]
                if (sum > maxSum || maxSum == Integer.MAX_VALUE)
                    maxSum = sum
            }
        }
    }

    println(maxSum)
}