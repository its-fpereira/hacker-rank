import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val listSize = sc.nextInt()

    val numbers = ArrayList<Float>()
    var positive = 0
    var negative = 0
    var zero = 0

    for (i in 0..listSize) {
        numbers.add(sc.nextFloat())
        if (numbers[i] > 0) positive++
        else if (numbers[i] < 0) negative++
        else zero++
    }

    println(BigDecimal(positive).divide(BigDecimal(numbers.size), 6, RoundingMode.HALF_UP))
    println(BigDecimal(negative).divide(BigDecimal(numbers.size), 6, RoundingMode.HALF_UP))
    println(BigDecimal(zero).divide(BigDecimal(numbers.size), 6, RoundingMode.HALF_UP))
}