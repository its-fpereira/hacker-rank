import java.util.*

fun main(args: Array<String>) {
    val stairLevels = Scanner(System.`in`).nextInt()

    for (i in 1..stairLevels) {
        println(" ".repeat(stairLevels).replaceRange(stairLevels - i, stairLevels, "#".repeat(i)))
    }
}