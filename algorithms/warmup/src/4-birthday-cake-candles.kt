import java.util.*

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val heights = PriorityQueue<Long>(Comparator.reverseOrder())

    for (i in 1..sc.nextInt()) {
        heights.add(sc.nextLong())
    }

    var sum = 0
    var tallest = 0L
    loop@ while (heights.size > 0) {
        val height = heights.remove()
        if (tallest == 0L) tallest = height
        when (height) {
            tallest -> sum += 1
            else -> break@loop
        }
    }

    print("$sum")
}