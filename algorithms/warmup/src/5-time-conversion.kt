import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

fun main(args: Array<String>) {
    val time = Scanner(System.`in`).nextLine()

    print(LocalTime.parse(time, DateTimeFormatter.ofPattern("hh:mm:ssa")).format(DateTimeFormatter.ofPattern("HH:mm:ss")))
}