import java.util.*

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val evenNumbers = PriorityQueue<Long>()
    for (i in 1..5) {
        evenNumbers.add(sc.nextLong())
    }

    var sumMax = 0L
    var sumMin = 0L
    while (evenNumbers.size > 0)
        when (evenNumbers.size) {
            1 -> sumMax += evenNumbers.remove()
            5 -> sumMin += evenNumbers.remove()
            else -> {
                val number = evenNumbers.remove()
                sumMax += number
                sumMin += number
            }
        }

    print("$sumMin $sumMax")
}